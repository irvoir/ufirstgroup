import React, { ReactElement } from "react";
import styled from "styled-components";
import { StateProvider } from "./state/state";
import Navigation from "./components/Navigation";
import Content from "./components/Content";
import Notification from "./components/Notification";
import { reducer, initialState } from "./state/store";

const Wrapper = styled.div`
  display: grid;
  overflow: hidden;
  background: #fef7ea;
  min-height: 100vh;
  min-width: 100vw;
  grid-template-columns: 2fr 190px 25px 8fr 2fr;
  grid-template-rows: 1fr minmax(465px, 500px) 1fr;
  grid-template-areas:
    ". . . . ."
    ". nav . cont ."
    ". . . . .";
`;

export const App = (): ReactElement => {
  return (
    <StateProvider initialState={initialState} reducer={reducer}>
      <Wrapper>
        <Navigation />
        <Content />
        <Notification />
      </Wrapper>
    </StateProvider>
  );
};
