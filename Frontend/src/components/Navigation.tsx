import React, { ReactElement, useState, useEffect } from "react";
import styled from "styled-components";
import { useStateValue } from "../state/state";

const Navigation = styled.div`
  grid-area: nav;
  border: 2px solid #282922;
  background: #c4e3db;
  border-radius: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  font-family: "Dinnextltpro", sans-serif;
  font-size: 22px;
  text-transform: uppercase;
  letter-spacing: 2px;
  position: relative;
  overflow: hidden;
`;
const Intro = styled.div`
  margin-top: 35px;
  cursor: pointer;
  display: flex;
  width: 100%;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
const Files = styled.div`
  margin-top: 20px;
  cursor: pointer;
  display: flex;
  width: 100%;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
const Create = styled.div`
  margin-top: 20px;
  cursor: pointer;
  display: flex;
  width: 100%;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
const Graphs = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  align-content: center;
  justify-content: center;
  margin-top: 20px;
  cursor: pointer;
`;
const Span = styled.span`
  user-select: none;
  display: flex;
`;

const SpanText = styled.span`
  user-select: none;
  display: flex;
  flex: 1;
  justify-content: center;
`;

const NavigationComponent = (): ReactElement => {
  const [{ route }, dispatch] = useStateValue();

  return (
    <Navigation>
      <Intro onClick={() => dispatch({ type: "CHANGE_ROUTE", route: "Intro" })}>
        <Span>{route === "Intro" ? "►" : ""}</Span>
        <SpanText>Intro</SpanText>
      </Intro>
      <Files onClick={() => dispatch({ type: "CHANGE_ROUTE", route: "Files" })}>
        <Span>{route === "Files" ? "►" : ""}</Span>
        <SpanText>Files</SpanText>
      </Files>
      <Create onClick={() => dispatch({ type: "CHANGE_ROUTE", route: "Create" })}>
        <Span>{route === "Create" ? "►" : ""}</Span>
        <SpanText>Create</SpanText>
      </Create>
      <Graphs onClick={() => dispatch({ type: "CHANGE_ROUTE", route: "Graphs" })}>
        <Span>{route === "Graphs" ? "►" : ""}</Span>
        <SpanText>Graphs</SpanText>
      </Graphs>
      <svg
        viewBox="0 0 390.9 241.7"
        style={{ position: "absolute", bottom: "-25px", left: "-25px" }}
      >
        <path
          fill="#0ED5A1"
          d="M273.4,40c-40-0.2-73-42.3-104-39.9c-47.3,3.6-58.9,41.7-94.8,42.3C38.7,43,12.6,46.6,4,61.9 c-18.6,33.1,34.2,69.7,34.7,90c0.6,28.6-28.5,36.4-22.6,54.2c7.5,22.5,49.8,33.5,90.5,32.5c30.6-0.8,53.6-18.8,84.5-27.8 c47.2-13.8,95.1,35.4,180.5-47.6c17.4-17,23.1-44.9,17-68.4C382.9,73.4,352.3,40.4,273.4,40z"
        ></path>
      </svg>
      <svg
        viewBox="0 0 735.1 806.8"
        id="shape"
        style={{ position: "absolute", bottom: "-115px", right: "-25px" }}
      >
        <path
          fill="#3D36FB"
          d="M720.9,146.8C716.6,42.5,583.1-50.6,493.3,31.3C427,91.6,389.7,133.6,331.7,152.6 c-68.7,22.5-138.4,33.9-185.9,84.9c-21.1,22.7-45.5,63.2-50,97.6c-5.8,44.1,30.7,63.2-22.3,123.7c-53,60.5-76.1,89.5-73.3,128.8 c3.4,49.2,64.3,80.5,101,81.6c65.9,1.9,65.8-15.6,100.3,14.2c48.4,41.9,17.6,102.2,56,119.1c48.8,21.4,130.7-32.4,190.2-101.6 c60.4-70.4,31.9-173.6,143.5-240.4C696.2,397.4,724.9,243.6,720.9,146.8z"
        ></path>
      </svg>
    </Navigation>
  );
};

export default NavigationComponent;
