import React, { useEffect, useReducer } from "react";
import { SVGTimelineIconMethod, SVGTimelineIconStatus } from "../assets/svgs";
import styled from "styled-components";

interface DistributionProps {
  data: number[];
  type: string;
  legend: number[];
  total: number;
}

interface LegendTextProps {
  disabled?: boolean;
}

interface LegendColorProps {
  color: string | number;
}

const GraphIcon = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Graph = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: space-evenly;
  flex-direction: column;
`;
const GraphInputData = styled.div`
  font-size: 15px;
  font-weight: 700;
`;

const Line = styled.div`
  width: 100%;
  height: 25px;
  border: 2px solid #282922;
  overflow: hidden;
  border-radius: 7px;
  display: flex;
  flex-direction: row;
`;

const LegendTop = styled.div`
  font-size: 15px;
  font-weight: 700;
  width: 100%;
  height: 25px;
  display: flex;
  justify-content: flex-end;
`;

const LegendBottom = styled.div`
  width: 100%;
  height: 25px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const LegendText = styled.div`
  font-size: 18px;
  text-transform: none;
  margin-left: 5px;
  margin-right: 5px;
  text-decoration: ${({ disabled }: LegendTextProps) =>
    disabled ? "line-through" : "none"};
`;

const LegendItem = styled.div`
  display: flex;
  position: relative;
  z-index: 1;
  flex-direction: row;
  cursor: default;
  user-select: none;
`;

const LegendColor = styled.div`
  width: 15px;
  height: 15px;
  border: 2px solid #282922;
  background: ${({ color }: LegendColorProps) => colorArray[+color]};
`;

const colorArray = [
  "#BADFDB",
  "#E03400",
  "#F0F9FF",
  "#5D282C",
  "#B4DAED",
  "#FFD853",
  "#ACD084",
  "#78523C"
];

const percent = (input: number, total: number) =>
  Math.round((input * 100) / total);

interface ActionProps {
  initial?: {
    data: number[];
    legend: number[];
    total: number;
  };
  data?: number[];
  legend?: number[];
  select?: number;
  selected?: number[];
  total?: number;
  update?: boolean;
  type: string;
}

export const initialState = {
  data: [],
  initial: {
    data: null,
    legend: null,
    total: 0
  },
  select: null,
  legend: [],
  selected: [],
  update: false,
  total: 0
};

const reducer = (state = initialState, action: ActionProps) => {
  switch (action.type) {
    case "INITIAL_DATA": {
      return {
        ...state,
        initial: action.initial
      };
    }
    case "UPDATED_DATA":
      return {
        ...state,
        data: action.data,
        legend: action.legend,
        total: action.total
      };
    case "SELECT": {
      return {
        ...state,
        select: action.select
      };
    }
    case "REARRANGE_DATA": {
      return {
        ...state,
        selected: action.selected
      };
    }
    case "UPDATE": {
      return {
        ...state,
        update: action.update
      };
    }
    default:
      return state;
  }
};

const Distribution = ({ data, type, legend, total }: DistributionProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (state.update) {
      let { data, total } = state.initial;
      let newData = data.slice();
      let newTotal = total;
      if (state.selected.length > 0) {
        for (let index = 0; index < state.selected.length; index += 1) {
          newTotal -= data[state.selected[index]];
          newData[state.selected[index]] = 0;
        }
      }
      dispatch({
        type: "UPDATED_DATA",
        data: newData,
        legend: state.initial.legend,
        total: newTotal
      });
      dispatch({ type: "UPDATE", update: false });
    }
  }, [state.update]);

  useEffect(() => {
    if (state.select !== null) {
      let reSelected = state.selected.slice();
      if (state.selected.includes(state.select)) {
        reSelected.splice(state.selected.indexOf(state.select), 1);
      } else {
        reSelected.push(state.select);
      }
      dispatch({ type: "SELECT", select: null });
      dispatch({ type: "UPDATE", update: true });
      dispatch({ type: "REARRANGE_DATA", selected: reSelected });
    }
  }, [state.select]);

  useEffect(() => {
    if (data.length > 0 && legend.length > 0 && total > 0) {
      dispatch({
        type: "INITIAL_DATA",
        initial: {
          data,
          legend,
          total
        }
      });
      dispatch({ type: "UPDATED_DATA", data, legend, total });
    }
  }, [data, legend]);

  return (
    <>
      <GraphIcon>
        {type === "method" ? (
          <SVGTimelineIconMethod />
        ) : (
          <SVGTimelineIconStatus />
        )}
        <GraphInputData>{state.total}</GraphInputData>
      </GraphIcon>
      <Graph>
        <LegendTop>
          {state.data.length === 0
            ? "Loading..."
            : `${state.data.length - state.selected.length} of ${
                state.initial.data.length
              } selected`}
        </LegendTop>
        <Line>
          {state.data.map((item, index) => {
            if (state.selected.includes(index)) return null;
            return (
              <div
                key={item + index}
                style={{
                  width: `${percent(item, state.total) === 0 ? 1 : percent(item, state.total)}%`,
                  borderRight:
                    index === state.data.length - 1 ? "" : "2px solid #282922",
                  backgroundColor: colorArray[index]
                }}
              />
            );
          })}
        </Line>
        <LegendBottom>
          {state.legend.map((item, index) => (
            <LegendItem
              key={item + index}
              onClick={() => dispatch({ type: "SELECT", select: index })}
            >
              <LegendColor color={"" + index} />
              <LegendText disabled={state.selected.includes(index)}>
                {state.legend[index]}
              </LegendText>
            </LegendItem>
          ))}
        </LegendBottom>
      </Graph>
    </>
  );
};

export default Distribution;
