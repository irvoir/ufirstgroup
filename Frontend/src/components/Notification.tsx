import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useStateValue } from "../state/state";
import useTimeout from "../hooks/useTimeout";

const Wrapper = styled.div`
  position: absolute;
  display: flex;
  flex: 1;
  right: 25px;
  top: 25px;
  height: 50px;
  padding-left: 15px;
  padding-right: 15px;
  font-family: "Dinnextltprop", sans-serif;
  font-size: 18px;
  background: #faf3e5;
  border: 2px solid #282922;
  border-radius: 7px;
  justify-content: center;
  align-content: center;
  align-items: center;
  box-shadow: 2px 2px 0px 0px #282922;
`;

const Notification = () => {
  const [{ notification }, globalDispatch] = useStateValue();
  const [remove, setRemove] = useState(true);
  useTimeout(() => setRemove(true), 7000, notification);

  useEffect(() => {
    if (remove) {
      globalDispatch({ type: "ADD_NOTIFICATION", notification: null });
      setRemove(false);
    }
  }, [notification, remove]);

  if (notification) {
    return (
      <Wrapper>
        {'❗'}
        {notification}
      </Wrapper>
    );
  }
  return null;
};
export default Notification;
