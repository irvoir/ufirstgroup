import React, { Fragment, useRef, useLayoutEffect, useReducer } from "react";
import styled from "styled-components";

interface TimelineProps {
  data: {
    [propName: string]: number;
  };
  total: number;
  name: string;
  time?: boolean;
}

interface BarHeightProps {
  height: number;
}

interface ActionProps {
  type: string;
  sparkline?: boolean;
  width?: number;
  height?: number;
  fill?: string;
  line?: string;
}

interface SparklineToggleProps {
  active: boolean;
}
interface SparklineProps {
  sparkline: boolean;
}

const TimelineTitle = styled.div`
  grid-column: 2/4;
  grid-row: 2/3;
  color: #282922;
  font-size: 15px;
  text-transform: none;
  font-weight: 700;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
const TimelineGraphContainer = styled.div`
  ${({ sparkline }: SparklineProps) =>
    !sparkline
      ? `
    grid-column: 1/5;
    grid-row: 3/4;
    display: grid;
    grid-template-columns: repeat(10, auto);
    align-items: stretch;
  `
      : `
    display: flex;
    flex: 1;
    grid-column: 1/5;
    grid-row: 3/4;
  `};
  overflow: hidden;
`;

const Bar = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  align-content: center;
  padding-bottom: 15px;
`;

const BarHeight = styled.div`
  width: 100%;
  background: #282922;
  height: ${({ height }: BarHeightProps) => (height ? `${height}%` : "25px")};
  &: hover {
    height: ${({ height }: BarHeightProps) => (height ? `${height + 25}%` : "25px")};
    background: #ED1250;
  }
`;

const Time = styled.div`
  font-size: 12px;
`;

const Requests = styled.div`
  color: #2d132c;
  font-size: 12px;
  font-weight: 800;
`;

const SparklineToggle = styled.div`
  text-transform: uppercase
  text-decoration: ${({ active }: SparklineToggleProps) =>
    active ? "" : "line-through"};
  cursor: default;
  user-select: none;
  &:hover {
    text-decoration: ${({ active }: SparklineToggleProps) =>
      !active ? "none" : "line-through"};
  }
`;

const percent = (input: number, total: number) =>
  Math.round((input * 100) / total);

const getY = (max: number, height: number, diff: number, value: number) =>
  parseFloat((height - (value * height) / max + diff).toFixed(2));

export const initialState = {
  sparkline: false,
  width: 0,
  height: 0,
  fill: "",
  line: ""
};

const reducer = (state = initialState, action: ActionProps) => {
  switch (action.type) {
    case "TOGGLE": {
      return {
        ...state,
        sparkline: !state.sparkline
      };
    }
    case "SET_RECT": {
      return {
        ...state,
        width: action.width,
        height: action.height
      };
    }
    case "SET_GRAPH": {
      return {
        ...state,
        fill: action.fill,
        line: action.line
      };
    }
    default:
      return state;
  }
};

const TimelineGraph = ({ data, total, name, time }: TimelineProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const graphRef: any = useRef();
  const offset = 85;

  useLayoutEffect(() => {
    if (graphRef && graphRef.current !== undefined) {
      let {
        width: svgWidth,
        height: svgHeight
      } = graphRef.current.getBoundingClientRect();
      svgWidth = svgWidth + offset;
      dispatch({ type: "SET_RECT", width: svgWidth, height: svgHeight });

      const values = Object.values(data);
      const height = (svgHeight - 4) / 2;
      const max = Math.max.apply(Math, values);
      let pathCoords = `M0 ${getY(max, height, 4, values[0]).toFixed(2) + offset}`;
      for (let index = 0; index < values.length; index += 1) {
        const x = (index * svgWidth) / values.length - 1;
        const y = getY(max, height, 4, values[index]);
        pathCoords += ` L ${x} ${y}`;
      }
      let fillCoords = `${pathCoords} V ${svgHeight / 2} L 0 ${svgHeight / 2} Z`;
      dispatch({ type: "SET_GRAPH", line: pathCoords, fill: fillCoords });
    }
  }, [data, graphRef]);

  return (
    <>
      <TimelineTitle>
        {name}
        <SparklineToggle
          onClick={() => dispatch({ type: "TOGGLE" })}
          active={state.sparkline}
        >
          Sparkline
        </SparklineToggle>
      </TimelineTitle>
      <TimelineGraphContainer ref={graphRef} sparkline={state.sparkline}>
        {state.sparkline ? (
          <svg
            width={state.width}
            height={state.height}
            strokeWidth={2}
            stroke="#282922"
          >
            <defs>
              <linearGradient id="Gradient5" x2="0%" y2="100%">
                <stop offset="0%" stopColor="#226B80"></stop>
                <stop offset="100%" stopColor="#9cf196"></stop>
              </linearGradient>
            </defs>
            <path d={state.fill} fill="url(#Gradient5)" stroke="none" />
            <path d={state.line} fill="none" />
          </svg>
        ) : (
          <>
            {Object.keys(data).map((item, index) => (
              <Fragment key={index + item}>
                <Bar>
                  <Requests>{data[item]}</Requests>
                  <BarHeight height={percent(data[item], total)} />
                  <Time>
                    {item}
                    {time ? "PM" : "B"}
                  </Time>
                </Bar>
              </Fragment>
            ))}
          </>
        )}
      </TimelineGraphContainer>
    </>
  );
};

export default TimelineGraph;
