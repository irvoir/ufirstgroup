import React, { ReactElement } from "react";
import styled from "styled-components";
import { useStateValue } from "../state/state";
import Intro from "../screens/Intro";
import Files from "../screens/Files";
import Create from "../screens/Create";
import Graphs from "../screens/Graphs";

const Content = styled.div`
  grid-area: cont;
  display: flex;
  flex: 1;
`;

const renderRoute = (route: string): ReactElement => {
  switch (route) {
    case "Intro":
      return <Intro />;
    case "Files":
      return <Files />;
    case "Create":
      return <Create />;
    case "Graphs":
      return <Graphs />;
    default:
      return <Intro />;
  }
};

const ContentComponent = (): ReactElement => {
  const [{ route }] = useStateValue() as any;
  return <Content>{renderRoute(route)}</Content>;
};

export default ContentComponent;
