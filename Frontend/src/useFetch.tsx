import { useEffect, useState } from "react";

export const useGraphFetch = (
  type: string,
  file: string,
  graphType: string,
  host: string
) => {
  let url: string = `${host}/${type}/${file}/${graphType}`;
  const [state, setState] = useState({ data: null, loading: true });
  useEffect(() => {
    setState(state => ({ data: state.data, loading: true }));
    try {
      if (file) {
        fetch(url)
          .then(stream => {
            return stream.json();
          })
          .then(json => {
            setState({ data: json, loading: false });
          });
      }
    } catch (error) {
      setState({ data: null, loading: false });
    }
  }, [url]);
  return state;
};

interface FileProps {
  data: {
    message: string[];
  };
  loading: boolean;
}

export const useFileFetch = (type: string, host: string, canFetch: boolean) => {
  let url: string = `${host}/${type}`;
  const defaultState: FileProps = {
    data: {
      message: []
    },
    loading: true
  };
  const [state, setState] = useState(defaultState);
  useEffect(() => {
    if (canFetch) {
      setState(state => ({ data: state.data, loading: true }));
      try {
        fetch(url)
          .then(stream => {
            return stream.json();
          })
          .then(json => {
            setState({ data: json, loading: false });
          });
      } catch (error) {
        setState({ data: { message: [] }, loading: false });
      }
    }
  }, [url, canFetch]);
  return state;
};

export const useFileDelete = (
  url: string,
  selectedFiles: string[],
  canDelete: boolean
) => {
  const [state, setState] = useState({
    deleted: null,
    loadingDelete: true
  });
  useEffect(() => {
    if (canDelete && selectedFiles.length > 0) {
      setState(state => ({
        deleted: state.deleted,
        loadingDelete: true
      }));
      try {
        fetch(url, {
          headers: {
            "Content-Type": "application/json"
          },
          method: "POST",
          body: JSON.stringify(selectedFiles)
        })
          .then(stream => {
            return stream.json();
          })
          .then(json => {
            setState({ deleted: json, loadingDelete: false });
          });
      } catch (error) {
        setState({ deleted: null, loadingDelete: false });
      }
    }
  }, [url, canDelete]);
  return state;
};

export const useDefaultFile = (url: string, create: boolean) => {
  const [state, setState] = useState({ data: null, loading: null });

  useEffect(() => {
    setState(state => ({ data: state.data, loading: true }));
    try {
      if (create) {
        fetch(url, {
          headers: { "Content-Type": "application/json" },
          method: "POST",
          body: JSON.stringify({ name: "default.json" })
        })
          .then(stream => {
            return stream.json();
          })
          .then(() => {
            setState({ data: true, loading: false });
          });
      }
    } catch (error) {
      setState({ data: true, loading: false });
    }
  }, [url, create]);
  return state;
};

export const useCustomFile = (url: string, options: any, create: boolean) => {
  const [state, setState] = useState({ data: null, loading: null });

  useEffect(() => {
    setState(state => ({ data: state.data, loading: true }));
    try {
      if (create) {
        fetch(url, {
          headers: { "Content-Type": "application/json" },
          method: "POST",
          body: JSON.stringify(options)
        })
          .then(stream => {
            return stream.json();
          })
          .then(() => {
            setState({ data: true, loading: false });
          });
      }
    } catch (error) {
      setState({ data: true, loading: false });
    }
  }, [url, create]);
  return state;
};
