interface ActionProps {
  type: string;
  viewFile: string;
  route: string;
  notification?: string;
}

export const initialState = {
  route: "Intro",
  viewFile: "",
  url: "https://ufirstgroup.herokuapp.com",
  notification: null
};

export const reducer = (state = initialState, action: ActionProps) => {
  switch (action.type) {
    case "CHANGE_ROUTE":
      return {
        ...state,
        route: action.route
      };
    case "VIEW_GRAPH":
      return {
        ...state,
        viewFile: action.viewFile
      };
    case "ADD_NOTIFICATION":
      return {
        ...state,
        notification: action.notification
      };
    default:
      return state;
  }
};
