import React, { ReactElement, useCallback, useEffect, useReducer } from "react";
import styled from "styled-components";
import { File, FilesBlob } from "../assets/svgs";
import { useFileFetch, useFileDelete } from "../useFetch";
import { useStateValue } from "../state/state";

interface ActionProps {
  files?: any[];
  file?: string;
  canSelect?: boolean;
  canView?: string;
  selectedFiles?: string[];
  openFile?: string;
  deleteFile?: string | string[];
  triggerDelete?: boolean;
  canFetch?: boolean;
  view?: string[];
  type: string;
}

interface RenderFileProps {
  select: any;
  item: any;
  isSelected: boolean;
}

interface LoadingOrNoFilesProps {
  loading: boolean;
  files: number;
}

interface ViewGraphProps {
  canView?: boolean;
}

interface OpenFileProps {
  canOpen: boolean;
}

interface DeleteFileProps {
  canDelete: boolean;
}

const Files = styled.div`
  width: 100%;
  border: 2px solid #282922;
  background: #f5ebd5;
  border-radius: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  font-family: "Dinnextltpro", sans-serif;
  font-size: 22px;
  text-transform: none;
  letter-spacing: 2px;
  position: relative;
  overflow: hidden;
`;

const Header = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 50px 10fr;
  border-bottom: 2px solid #282922;
  text-transform: none;
`;

const Action = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;

const ActionButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 13px;
  height: 13px;
  border: 2px solid #282922;
  border-radius: 25px;
  font-size: 30px;
  margin-left: 2px;
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  color: #282922;
  user-select: none;
`;

const FilesContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex: 1;
  justify-content: flex-start;
  align-items: flex-start;
  align-content: flex-start;
  flex-direction: column;
  font-size: 15px;
  overflow: hidden;
  position: relative;
`;

const FileCreation = styled.div`
  text-transform: none;
  margin-left: 15px;
  margin-top: 15px;
  margin-bottom: 15px;
  user-select: none;
`;

const Folder = styled.div`
  grid-column: 1 / 5;
  letter-spacing: 1.2px;
  margin-top: 18px;
  align-self: center;
  display: flex;
  flex: 1;
`;

const ViewGraph = styled.div`
  display: flex;
  cursor: pointer;
  user-select: none;
  justify-content: center;
  align-items: center;
  align-content: center;
  border: 2px solid #282922;
  border-radius: 5px;
  margin-left: 15px;
  margin-right: 15px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  opacity: ${({ canView }: ViewGraphProps) => (canView ? "1" : "0.5")}
    ${({ canView }: ViewGraphProps) =>
      canView
        ? `
    &:hover {
      background: #282922;
      color: white;
    }
  `
        : ""};
`;

const DeleteFile = styled.div`
  display: flex;
  cursor: pointer;
  user-select: none;
  justify-content: center;
  align-items: center;
  align-content: center;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  opacity: ${({ canDelete }: DeleteFileProps) => (canDelete ? "1" : "0.5")}
    ${({ canDelete }: DeleteFileProps) =>
      canDelete
        ? `
    &:hover {
      background: #282922;
      color: white;
    }
  `
        : ""};
`;

const OpenFile = styled.div`
  display: flex;
  cursor: pointer;
  user-select: none;
  justify-content: center;
  align-items: center;
  align-content: center;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  opacity: ${({ canOpen }: OpenFileProps) => (canOpen ? "1" : "0.5")}
    ${({ canOpen }: OpenFileProps) =>
      canOpen
        ? `
    &:hover {
      background: #282922;
      color: white;
    }
  `
        : ""};
`;

const FilesContainer = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fill, 125px);
  grid-template-rows: repeat(auto-fill, 2fr);
  grid-gap: 10px;
  margin-left: 15px;
`;

const Item = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  aling-content: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const FileName = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 15px;
  user-select: none;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 15px;
  position: relative;
  overflow: hidden;
`;

const RenderFile = ({ select, item, isSelected }: RenderFileProps) => (
  <Item onClick={() => select(item)}>
    <File selected={isSelected} />
    <FileName>{item}</FileName>
  </Item>
);

const LoadingOrNoFiles = ({ loading, files }: LoadingOrNoFilesProps) => {
  if (loading) {
    <Folder>{loading ? "Loading." : "No files found. Create some!"}</Folder>;
  }
  return (
    <Folder>{files > 0 ? "Loading." : "No files found. Create some!"}</Folder>
  );
};

const initialState = {
  files: [],
  file: [],
  canView: "",
  canSelect: false,
  openFile: "",
  deleteFile: [],
  selectedFiles: [],
  triggerDelete: false,
  canFetch: true
};

const fileReducer = (state = initialState, action: ActionProps) => {
  const { selectedFiles } = state;
  switch (action.type) {
    case "GET_FILES": {
      return {
        ...state,
        files: action.files
      };
    }
    case "SELECT_A_FILE": {
      let newSelected = [];
      if (selectedFiles.includes(action.file)) {
        newSelected = selectedFiles.filter(item => item !== action.file);
        return {
          ...state,
          selectedFiles: newSelected,
          canSelect: newSelected.length === 1
        };
      }
      newSelected = [action.file, ...selectedFiles];
      return {
        ...state,
        selectedFiles: newSelected,
        canSelect: newSelected.length === 1
      };
    }
    case "OPEN_FILE": {
      return {
        ...state,
        openFile: selectedFiles.length === 1 ? selectedFiles : ""
      };
    }
    case "DELETE_FILE": {
      return {
        ...state,
        deleteFile: selectedFiles
      };
    }
    case "CLEAR_SELECTED": {
      return {
        ...state,
        selectedFiles: [],
        openFile: "",
        canSelect: false,
        triggerDelete: false,
        deleteFile: [],
        canView: ""
      };
    }
    case "TRIGGER_DELETE": {
      return {
        ...state,
        triggerDelete: true
      };
    }
    case "TRIGGER_FETCH": {
      return {
        ...state,
        canFetch: action.canFetch
      };
    }
    case "VIEW_GRAPH": {
      return {
        ...state,
        canView: state.selectedFiles[0]
      };
    }
    default:
      return state;
  }
};

const FilesComponent = (): ReactElement => {
  const [{ url }, globalDispatch] = useStateValue();
  const [state, dispatch] = useReducer(fileReducer, initialState);
  const { data, loading } = useFileFetch("files", url, state.canFetch);
  const { deleted, loadingDelete } = useFileDelete(
    `${url}/deletefiles`,
    state.selectedFiles,
    state.triggerDelete
  );

  useEffect(() => {
    if (!loadingDelete) {
      dispatch({ type: "TRIGGER_FETCH", canFetch: true });
    }
  }, [loadingDelete]);

  useEffect(() => {
    if (state.canView.length > 0) {
      globalDispatch({ type: "VIEW_GRAPH", viewFile: state.canView });
      globalDispatch({
        type: "ADD_NOTIFICATION",
        notification: `${state.canView} has been selected.`
      });
      globalDispatch({ type: "CHANGE_ROUTE", route: "Graphs" });
      dispatch({ type: "CLEAR_SELECTED" });
    }
  }, [state.canView]);

  useEffect(() => {
    if (state.deleteFile.length > 0 && !state.canFetch) {
      dispatch({ type: "TRIGGER_DELETE" });
      globalDispatch({
        type: "ADD_NOTIFICATION",
        notification: `Your files will be deleted.`
      });
      globalDispatch({
        type: "VIEW_GRAPH",
        viewFile: ""
      });
    }
  }, [state.deleteFile, state.triggerDelete]);

  useEffect(() => {
    if (state.openFile) {
      const [file] = state.openFile;
      window.location.replace(`${url}/openfile/${file}`);
      dispatch({ type: "CLEAR_SELECTED" });
    }
  }, [state.openFile]);

  useEffect(() => {
    dispatch({ type: "GET_FILES", files: data.message });
    dispatch({ type: "TRIGGER_FETCH", canFetch: false });
    dispatch({ type: "CLEAR_SELECTED" });
  }, [loading]);

  const selectCallback = useCallback(
    n => {
      dispatch({ type: "SELECT_A_FILE", file: n });
    },
    [state.selectedFiles]
  );

  return (
    <Files>
      <Header>
        <Action>
          <ActionButton />
          <ActionButton />
        </Action>
        <Title>Files</Title>
      </Header>
      <FilesBlob />
      <FilesContent>
        <FileCreation>Last 7 days</FileCreation>
        {state.files.length ? (
          <FilesContainer>
            {state.files.map(item => (
              <RenderFile
                select={selectCallback}
                item={item}
                key={item}
                isSelected={state.selectedFiles.includes(item)}
              />
            ))}
          </FilesContainer>
        ) : (
          <LoadingOrNoFiles loading files={state.files.length} />
        )}
      </FilesContent>
      <ButtonContainer>
        <DeleteFile
          canDelete={state.selectedFiles.length > 0}
          onClick={() => dispatch({ type: "DELETE_FILE" })}
        >
          Delete
        </DeleteFile>
        <ViewGraph
          canView={state.canSelect}
          onClick={() => {
            if (state.canSelect) {
              dispatch({ type: "VIEW_GRAPH" });
            }
          }}
        >
          View Graph
        </ViewGraph>
        <OpenFile
          canOpen={state.canSelect}
          onClick={() => {
            if (state.canSelect) {
              dispatch({ type: "OPEN_FILE" });
            }
          }}
        >
          Open
        </OpenFile>
      </ButtonContainer>
    </Files>
  );
};

export default FilesComponent;
