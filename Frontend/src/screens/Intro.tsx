import React, { ReactElement } from "react";
import styled from "styled-components";
import { introTitle, intro } from "../text/intro";
import { IntroBlob } from "../assets/svgs";

const Intro = styled.div`
  width: 100%;
  border: 2px solid #282922;
  background: #f6d79b;
  border-radius: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  font-family: "Dinnextltpro", sans-serif;
  font-size: 22px;
  text-transform: uppercase;
  letter-spacing: 2px;
  overflow: hidden;
  position: relative;
`;

const Header = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 50px 10fr;
  border-bottom: 2px solid #282922;
  text-transform: none;
  background: #f6d79b;
`;

const Action = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;

const ActionButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 13px;
  height: 13px;
  border: 2px solid #282922;
  border-radius: 25px;
  font-size: 30px;
  margin-left: 2px;
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

const IntroContent = styled.div`
  width: 100%;
  height: 100%;
  margin-left: 100px;
  margin-top: 50px;
`;

const TextTitle = styled.div`
  font-weight: 700;
`;

const Text = styled.div`
  margin-top: 25px;
  margin-bottom: 10px;
  font-weight: 500;
`;

const SmallerText = styled.div`
  margin-top: 10px;
  margin-left: 15px;
  font-weight: 500;
  font-size: 18px;
`;

const IntroComponent = (): ReactElement => {
  return (
    <Intro>
      <Header>
        <Action>
          <ActionButton />
          <ActionButton />
        </Action>
        <Title>Intro</Title>
      </Header>
      <IntroContent>
        <IntroBlob />
        <TextTitle>{introTitle}</TextTitle>
        <Text>{intro}</Text>
        <TextTitle>How to use:</TextTitle>
        <SmallerText>1. Go to files.</SmallerText>
        <SmallerText>2. Select a file. If there is no file present, create one.</SmallerText>
        <SmallerText>3. Go to Graphs and wait.</SmallerText>
        <SmallerText>4. Alternatively if there is no file, go to Create</SmallerText>
      </IntroContent>
    </Intro>
  );
};

export default IntroComponent;
