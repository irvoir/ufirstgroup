import React, { useState, ReactElement, useCallback } from "react";
import styled from "styled-components";
import CreateWizard from "../screens/Files/CreationWizard";
import CreateDefault from "../screens/Files/CreateDefault";
import {CreateBlob} from '../assets/svgs';

interface OptionButtonProps {
  select: boolean;
}

interface CreateContentProps {
  navigate: boolean;
}

const Create = styled.div`
  width: 100%;
  border: 2px solid #282922;
  background: #45969b;
  border-radius: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  font-family: "Dinnextltpro", sans-serif;
  font-size: 22px;
  text-transform: uppercase;
  letter-spacing: 2px;
`;

const Header = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 50px 10fr;
  border-bottom: 2px solid #282922;
  text-transform: none;
`;

const Action = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;

const ActionButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 13px;
  height: 13px;
  border: 2px solid #282922;
  border-radius: 25px;
  font-size: 30px;
  margin-left: 2px;
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  color: #282922;
`;

const CreateContent = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;
  display: grid;
  flex: 1;
  ${({ navigate }: CreateContentProps) =>
    navigate
      ? ""
      : `
  grid-template-rows: 1fr 1fr 1fr;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-areas:
    ". .        ."
    ". center   ."
    ". .   button";
  `};
`;

const Options = styled.div`
  grid-area: center;
  display: flex;
  justify-content: space-around;
  aling-items: center;
  flex-direction: column;
  box-sizing: border-box;
`;

const Option = styled.div`
  font-size: 18px;
  cursor: default;
  display: flex;
  flex-direction: row;
  justify-content: center;
  aling-items: center;
  box-sizing: border-box;
`;

const OptionText = styled.div`
  user-select: none;
  border: 2px solid #45969b;
  padding: 3px;
  &:hover {
    border: 2px dashed #282922;
  }
`;

const Buttons = styled.div`
  grid-area: button;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  padding-bottom: 35px;
`;

const OptionButton = styled.div`
  cursor: pointer;
  user-select: none;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  opacity: ${({ select }: OptionButtonProps) => (select ? "1" : "0.5")}
    ${({ select }: OptionButtonProps) =>
      select
        ? `
    &:hover {
      background: #282922;
      color: white;
    }
  `
        : ""};
`;

const CenterChar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: #282922;
  font-size: 22px;
  margin-right: 10px;
`;

const RenderOptions = ({ navigate, page, setPage }) => {
  switch (navigate) {
    case 1:
      return <CreateDefault />;
    case 2:
      return <CreateWizard />;
    default:
      return (
        <>
          <Options>
            <Option onClick={() => setPage(1)}>
              <CenterChar>{page === 1 ? "⊞" : "⊟"}</CenterChar>
              <OptionText>Default file</OptionText>
            </Option>
            <Option onClick={() => setPage(2)}>
              <CenterChar>{page === 2 ? "⊞" : "⊟"}</CenterChar>
              <OptionText>Custom file</OptionText>
            </Option>
          </Options>
          <Buttons>
            <OptionButton
              select={page >= 1}
              onClick={() => setPage(page, true)}
            >
              Create
            </OptionButton>
          </Buttons>
        </>
      );
  }
};

const CreateComponent = (): ReactElement => {
  const [page, setPage] = useState(0);
  const [navigate, setNavigate] = useState(0);

  const callback = useCallback(
    (page, go) => {
      setPage(page);
      if (go) {
        setNavigate(page);
      }
    },
    [page]
  );

  return (
    <Create>
      <Header>
        <Action>
          <ActionButton />
          <ActionButton />
        </Action>
        <Title>Create files</Title>
      </Header>
      <CreateContent navigate={navigate >= 1}>
        <CreateBlob />
        <RenderOptions navigate={navigate} page={page} setPage={callback} />
      </CreateContent>
    </Create>
  );
};

export default CreateComponent;
