import React, { ReactElement, useEffect, useReducer } from "react";
import styled from "styled-components";
import { useGraphFetch } from "../useFetch";
import Distribution from "../components/DistributionGraph";
import TimelineGraph from "../components/TimelineGraph";
import { GraphBlob } from "../assets/svgs";
import { useStateValue } from "../state/state";

interface ActionProps {
  type: string;
  file?: string;
  methodData?: any;
  statusData?: any;
  requestData?: {};
  timeData?: {};
  totals?: {};
}

const Graphs = styled.div`
  width: 100%;
  border: 2px solid #282922;
  background: #9cf196;
  border-radius: 7px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  font-family: "Dinnextltpro", sans-serif;
  font-size: 22px;
  text-transform: uppercase;
  letter-spacing: 2px;
  overflow: hidden;
  position: relative;
`;

const Header = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 50px 10fr;
  border-bottom: 2px solid #282922;
  text-transform: none;
`;

const Action = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;

const ActionButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 13px;
  height: 13px;
  border: 2px solid #282922;
  border-radius: 25px;
  font-size: 30px;
  margin-left: 2px;
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  color: #282922;
`;

const GraphsContentDistributon = styled.div`
  width: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const DistributionGraphStatus = styled.div`
  flex: 1;
  display: grid;
  grid-template-rows: 0px minmax(0px, 1fr) minmax(0px, 35px);
  grid-template-columns: minmax(0px, 35px) minmax(0px, 1fr) minmax(0px, 35px);
  border-bottom: 2px solid #282922;
  overflow: hidden;
`;

const DistributionGraphMethod = styled.div`
  flex: 1;
  display: grid;
  grid-template-rows: 0px minmax(0px, 1fr) minmax(0px, 35px);
  grid-template-columns: minmax(0px, 35px) minmax(0px, 1fr) minmax(0px, 35px);
`;

const DistributionGraph = styled.div`
  grid-column: 2 / 3;
  grid-row: 2 / 3;
  display: grid;
  grid-template-columns: 1fr 10fr;
`;

const Paginator = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  bottom: 10px;
  overflow: hidden;
`;

interface ArrowsProps {
  isNotLoading: boolean;
}

const Arrows = styled.div`
  border: 2px solid #282922;
  margin-left: 20px;
  line-height: 20px;
  cursor: pointer;
  user-select: none;
  opacity: ${({ isNotLoading }: ArrowsProps) => (isNotLoading ? "1" : "0.5")};
  ${({ isNotLoading }: ArrowsProps) =>
    isNotLoading
      ? `
    &: hover {
      color: #ED1250;
    }
  `
      : null};
`;

const GraphsContentTimeline = styled.div`
  width: 100%;
  flex: 1;
  display: grid;
  grid-template-rows: 25px 25px 1fr 25px;
  grid-template-columns: 25px 25px 1fr 25px;
`;

const renderPage = (state: any) => {
  const requestTitle = "⇅ Requests per minute";
  const successTitle = "✓ Success rate 200 and size < 1000B";
  switch (state.page) {
    case 1:
      return (
        <GraphsContentDistributon>
          <DistributionGraphStatus>
            <DistributionGraph>
              <Distribution
                type={"status"}
                data={state.statusData.data}
                legend={state.statusData.legend}
                total={state.totals.statusData}
              />
            </DistributionGraph>
          </DistributionGraphStatus>
          <DistributionGraphMethod>
            <DistributionGraph>
              <Distribution
                type={"method"}
                data={state.methodData.data}
                legend={state.methodData.legend}
                total={state.totals.methodData}
              />
            </DistributionGraph>
          </DistributionGraphMethod>
        </GraphsContentDistributon>
      );
    case 2:
      return (
        <GraphsContentTimeline>
          <TimelineGraph
            data={state.timeData}
            total={state.totals.requestData}
            name={requestTitle}
            time
          />
        </GraphsContentTimeline>
      );
    case 3:
      return (
        <GraphsContentTimeline>
          <TimelineGraph
            data={state.requestData}
            total={state.totals.distributionData}
            name={successTitle}
          />
        </GraphsContentTimeline>
      );
    default:
  }
};

const initialState = {
  page: 1,
  file: null,
  methodData: {
    data: [],
    legend: []
  },
  statusData: {
    data: [],
    legend: []
  },
  timeData: {},
  requestData: {},
  totals: {
    methodData: 0,
    statusData: 0,
    requestData: 0,
    distributionData: 0
  }
};

const graphReducer = (state = initialState, action: ActionProps) => {
  switch (action.type) {
    case "INCREMENT_PAGE":
      return {
        ...state,
        page: state.page < 3 ? state.page + 1 : state.page
      };
    case "DECREMENT_PAGE":
      return {
        ...state,
        page: state.page > 1 ? state.page - 1 : state.page
      };
    case "SET_SELECTED_PAGE":
      return {
        ...state,
        file: action.file
      };
    case "SET_METHOD_GRAPH":
      return {
        ...state,
        methodData: action.methodData
      };
    case "SET_STATUS_GRAPH":
      return {
        ...state,
        statusData: action.statusData
      };
    case "SET_ALL_TOTALS":
      return {
        ...state,
        totals: action.totals
      };
    case "SET_TIME_GRAPH":
      return {
        ...state,
        timeData: action.timeData
      };
    case "SET_REQUEST_GRAPH":
      return {
        ...state,
        requestData: action.requestData
      };
    default:
  }
};

const GraphsComponent = (): ReactElement => {
  const [state, dispatch] = useReducer(graphReducer, initialState);
  const [{ url, viewFile }] = useStateValue();

  const { data: serverMethodData, loading: loadingMethod } = useGraphFetch(
    "graph",
    state.file,
    "method",
    url
  );
  const { data: serverStatusData, loading: loadingStatus } = useGraphFetch(
    "graph",
    state.file,
    "status",
    url
  );
  const { data: requestData, loading: loadingRequest } = useGraphFetch(
    "graph",
    state.file,
    "request",
    url
  );
  const { data: timeData, loading: loadingTime } = useGraphFetch(
    "graph",
    state.file,
    "time",
    url
  );

  useEffect(() => {
    dispatch({ type: "SET_SELECTED_PAGE", file: viewFile });
  }, [viewFile]);

  const isNotLoading =
    !loadingMethod && !loadingStatus && !loadingRequest && !loadingTime;

  useEffect(() => {
    if (isNotLoading) {
      dispatch({
        type: "SET_METHOD_GRAPH",
        methodData: {
          data: serverMethodData.message.data,
          legend: serverMethodData.message.legend
        }
      });
      dispatch({
        type: "SET_STATUS_GRAPH",
        statusData: {
          data: serverStatusData.message.data,
          legend: serverStatusData.message.legend
        }
      });
      dispatch({
        type: "SET_REQUEST_GRAPH",
        requestData: requestData.message.data
      });
      dispatch({
        type: "SET_TIME_GRAPH",
        timeData: timeData.message.data
      });

      dispatch({
        type: "SET_ALL_TOTALS",
        totals: {
          methodData: serverMethodData.message.total,
          statusData: serverStatusData.message.total,
          requestData: requestData.message.total,
          distributionData: timeData.message.total
        }
      });
    }
  }, [loadingMethod, loadingStatus, loadingRequest, loadingTime]);

  return (
    <Graphs>
      <Header>
        <Action>
          <ActionButton />
          <ActionButton />
        </Action>
        <Title>{`Graphs - ${
          state.file ? state.file : "No file selected or created."
        }`}</Title>
      </Header>
      {renderPage(state)}
      <Paginator>
        <Arrows
          isNotLoading={isNotLoading}
          onClick={() => dispatch({ type: "DECREMENT_PAGE" })}
        >
          ◄
        </Arrows>
        <Arrows
          isNotLoading={isNotLoading}
          onClick={() => {
            if (isNotLoading) dispatch({ type: "INCREMENT_PAGE" });
          }}
        >
          ►
        </Arrows>
      </Paginator>
      <GraphBlob />
    </Graphs>
  );
};

export default GraphsComponent;
