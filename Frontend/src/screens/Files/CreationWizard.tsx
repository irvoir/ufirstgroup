import React, { ReactElement, useReducer, useState, useEffect } from "react";
import styled from "styled-components";
import { NewFile } from "../../assets/svgs";
import { useForm } from "../../hooks/useForm";
import { useStateValue } from "../../state/state";
import { useCustomFile } from "../../useFetch";

interface ActionButtonProps {
  selected?: boolean;
}

interface ActionProps {
  options?: {
    pretty: boolean;
    paginator: boolean;
    fields: boolean;
    range: boolean;
    name: string;
    min: number;
    max: number;
  };
  canCreate?: boolean;
  create?: boolean;
  min?: number;
  max?: number;
  name?: string;
  type: string;
}

const CreateFile = styled.div`
  display: flex;
  cursor: pointer;
  user-select: none;
  justify-content: center;
  align-items: center;
  align-content: center;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  opacity: ${({ canCreate }: any) => (canCreate ? "1" : "0.5")}
    ${({ canCreate }: any) =>
      canCreate
        ? `
    &:hover {
      background: #282922;
      color: white;
    }
  `
        : ""};
`;

const Action = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;

const ActionButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  width: 13px;
  height: 13px;
  border: 2px solid #282922;
  border-radius: 25px;
  font-size: 30px;
  margin-left: 2px;
  background: ${({ selected }: ActionButtonProps) =>
    selected ? "#282922" : "#f5ebd5"};
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
  color: #282922;
`;

const CreationWizard = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 2fr minmax(1fr, 750px) 2fr;
  grid-template-rows: 50px minmax(1fr, 450px) 50px;
  grid-template-areas:
    ". .  ."
    ". wiz ."
    ". . .";
`;

const Wizard = styled.div`
  grid-area: wiz;
  position: relative;
  z-index: 3;
  display: grid;
  grid-template-columns: 20px 2fr 15px 10fr;
  grid-template-rows: auto 20px 1fr 20px 50px 10px;
  grid-template-areas:
    "header header header header"
    ". . . ."
    ". icon . input"
    ". . . ."
    "buttons buttons buttons buttons"
    ". . . .";
  border: 2px solid #282922;
  background: #f5ebd5;
  border-radius: 7px;
  box-shadow: 4px 4px 0px 0px #282922;
`;
const WizardTitle = styled.div``;

const Icon = styled.div`
  grid-area: icon;
`;
const WizardInput = styled.div`
  grid-area: input;
  font-size: 20px;
  font-weight: 500;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;
const WizardHeader = styled.div`
  grid-area: header;
  border-bottom: 2px solid #282922;
  display: grid;
  grid-template-columns: 50px 10fr;
`;

const Option = styled.div`
  margin-top: 15px;
  margin-left: 15px;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  align-content: center;
  cursor: pointer;
  height: 25px;
`;

interface OptionTextProps {
  disabled: boolean;
}

const OptionText = styled.div`
  margin-left: 15px;
  user-select: none;
  text-decoration: ${({ disabled }: OptionTextProps) =>
    disabled ? "line-through" : "none"};
`;
const ButtonArea = styled.div`
  grid-area: buttons;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
`;

const Input = styled.input`
  font-family: "Dinnextltpro", sans-serif;
  background: none;
  outline: none;
  border: none;
  font-size: 22px;
`;

const initialState = {
  options: {
    pretty: true,
    paginator: false,
    fields: false,
    range: false,
    name: "",
    min: 0,
    max: 0
  },
  canCreate: false,
  create: false
};

function reducer(state = initialState, action: ActionProps) {
  switch (action.type) {
    case "SET_PRETTY":
      return {
        ...state,
        options: {
          ...state.options,
          pretty: true
        }
      };
    case "SET_PAGINATOR":
      return {
        ...state,
        options: {
          ...state.options,
          paginator: false
        }
      };
    case "SET_FIELDS":
      return {
        ...state,
        options: {
          ...state.options,
          fields: false
        }
      };
    case "SET_RANGE":
      return {
        ...state,
        options: {
          ...state.options,
          range: !state.options.range
        }
      };
    case "CAN_CREATE_FILE":
      return {
        ...state,
        canCreate: action.canCreate
      };
    case "CREATE_FILE": {
      return {
        ...state,
        create: action.create
      };
    }
    case "UPDATE_OPTIONS": {
      return {
        ...state,
        options: {
          ...state.options,
          name: action.name,
          min: action.min || 0,
          max: action.max || 0
        }
      };
    }
    default:
      return state;
  }
}

const CreationWizardComponent = (): ReactElement => {
  const [{ url }, globalDispatch] = useStateValue();
  const [state, dispatch] = useReducer(reducer, initialState);
  const [values, handleChange] = useForm({ name: "", min: 0, max: 0 });

  const { data, loading } = useCustomFile(
    `${url}/newcustomfile/`,
    state.options,
    state.create
  );

  useEffect(() => {
    if (state.create && loading) {
      globalDispatch({ type: "ADD_NOTIFICATION", notification: "Loading..." });
    }
    if (data && !loading) {
      globalDispatch({
        type: "ADD_NOTIFICATION",
        notification: `Your file: ${values.name} was created.`
      });
      dispatch({ type: "CREATE_FILE", create: false });
    }
  }, [data, loading]);

  useEffect(() => {
    const include = values.name.includes(".json");
    if (include) {
      dispatch({ type: "CAN_CREATE_FILE", canCreate: true });
      dispatch({
        type: "UPDATE_OPTIONS",
        name: values.name
      });
      if (+values.min > +values.max) {
        dispatch({ type: "CAN_CREATE_FILE", canCreate: false });
      } else {
        if (+values.min >= 0 && +values.max <= 47748 && state.options.range) {
          dispatch({
            type: "UPDATE_OPTIONS",
            name: values.name,
            min: +values.min,
            max: +values.max
          });
        }
      }
    } else {
      dispatch({ type: "CAN_CREATE_FILE", canCreate: false });
    }
  }, [values.name, values.min, values.max, state.options.range]);

  return (
    <CreationWizard>
      <Wizard>
        <WizardHeader>
          <Action>
            <ActionButton />
          </Action>
          <Title>
            <Input
              placeholder="Enter file name here..."
              value={values.name}
              onChange={handleChange}
              name="name"
              maxLength={20}
            />
          </Title>
        </WizardHeader>
        <Icon>
          <NewFile />
        </Icon>
        <WizardInput>
          <WizardTitle>Create your custom JSON file</WizardTitle>
          <Option onClick={() => dispatch({ type: "SET_PRETTY" })}>
            <ActionButton selected={state.options.pretty} />
            <OptionText disabled={false}>Pretty</OptionText>
          </Option>
          <Option onClick={() => dispatch({ type: "SET_PAGINATOR" })}>
            <ActionButton selected={state.options.paginator} />
            <OptionText disabled={true}>Paginator</OptionText>
          </Option>
          <Option onClick={() => dispatch({ type: "SET_FIELDS" })}>
            <ActionButton selected={state.options.fields} />
            <OptionText disabled={true}>Fields</OptionText>
          </Option>
          <Option>
            <ActionButton
              selected={state.options.range}
              onClick={() => dispatch({ type: "SET_RANGE" })}
            />
            <OptionText
              disabled={false}
              onClick={() => dispatch({ type: "SET_RANGE" })}
            >
              Range
            </OptionText>
            {state.options.range ? (
              <div style={{ display: "flex", flex: 1, marginLeft: "25px" }}>
                <Input
                  placeholder="Min"
                  value={values.min}
                  type="number"
                  onChange={handleChange}
                  name="min"
                  maxLength={20}
                  style={{
                    borderBottom: "2px solid #282922",
                    width: "75px",
                    height: "100%"
                  }}
                />
                <Input
                  placeholder="Max"
                  value={values.max}
                  type="number"
                  onChange={handleChange}
                  name="max"
                  maxLength={20}
                  style={{
                    marginLeft: "25px",
                    borderBottom: "2px solid #282922",
                    width: "75px",
                    height: "100%"
                  }}
                />
              </div>
            ) : null}
          </Option>
        </WizardInput>
        <ButtonArea>
          <CreateFile
            canCreate={state.canCreate}
            onClick={() => {
              if (state.canCreate) {
                dispatch({ type: "CREATE_FILE", create: true });
              }
            }}
          >
            Create
          </CreateFile>
        </ButtonArea>
      </Wizard>
    </CreationWizard>
  );
};
export default CreationWizardComponent;
