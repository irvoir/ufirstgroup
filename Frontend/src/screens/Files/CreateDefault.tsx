import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDefaultFile } from "../../useFetch";
import { useStateValue } from "../../state/state";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  display: grid;
  flex: 1;
  grid-template-rows: 100px 1fr 1fr;
  grid-template-columns: 100px 1fr 100px;
  grid-template-areas:
    ". .      ."
    ". center ."
    ". .      .";
`;

const Center = styled.div`
  grid-area: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 22px;
`;

const Title = styled.div`
  text-decoration: underline;
`;

const Name = styled.div`
  padding: 5px;
  text-transform: none;
  user-select: none;

  cursor: pointer;
  user-select: none;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  background: #282922;
  color: white;
`;

const Description = styled.ul`
  font-size: 18px;
  text-transform: none;
`;

const Input = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
`;
const Button = styled.div`
  margin-left: 15px;
  padding: 5px;
  text-transform: none;
  user-select: none;

  cursor: pointer;
  user-select: none;
  border: 2px solid #282922;
  border-radius: 5px;
  padding: 2px 7px 2px 7px;
  transition: 0.15s all ease-out;
  &:hover {
    background: #282922;
    color: white;
  }
`;

const CreateDefault = () => {
  const [{ url }, globalDispatch] = useStateValue();
  const [create, setCreate] = useState(false);
  const { data, loading } = useDefaultFile(`${url}/newfile`, create);

  useEffect(() => {
    if (!loading && data) {
      globalDispatch({
        type: "ADD_NOTIFICATION",
        notification: "File created."
      });
    }
  }, [loading, data]);

  useEffect(() => {
    if (create) {
      globalDispatch({ type: "ADD_NOTIFICATION", notification: "Loading..." });
      setCreate(false);
    }
  }, [create]);

  return (
    <Wrapper>
      <Center>
        <Title>Generate the default file</Title>
        <Description>
          <li>47,748 total requests</li>
          <li>46,014 GET requests</li>
          <li>1,622 POST requests</li>
          <li>107 HEAD requests</li>
          <li>6 invalid requests</li>
        </Description>
        <Input>
          <Name>default.json</Name>
          <Button onClick={() => setCreate(true)}>Create</Button>
        </Input>
      </Center>
    </Wrapper>
  );
};

export default CreateDefault;
