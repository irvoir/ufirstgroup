import { useState } from "react";

export const useForm = (initialValue: any) => {
  const [values, setValues] = useState(initialValue);
  return [
    values,
    (event: any) => {
      setValues({
        ...values,
        [event.target.name]: event.target.value
      });
    }
  ];
};
