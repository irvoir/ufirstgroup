import { useEffect, useRef } from "react";

function useTimeout(callback: any, delay: number, msg: string) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null && msg) {
      let id = setTimeout(tick, delay);
      return () => clearTimeout(id);
    }
  }, [delay, msg]);
}

export default useTimeout;
