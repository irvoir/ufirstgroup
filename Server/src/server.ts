import * as Koa from "koa";
import * as Router from "koa-router";
import * as Logger from "koa-logger";
import * as Json from "koa-json";
import * as Cors from "@koa/cors";
import * as Body from "koa-bodyparser";
import * as Static from "koa-static";
import { createFile, createCustom } from "./files/createfile";
import { getFileNames, getFile } from "./files/getfile";
import { openFile } from "./files/openfile";
import {
  getMethodGraphData,
  getStatusGraphData,
  getTimeGraphData,
  getRequestGraphData
} from "./graphs/graphs";
import { deleteFiles } from "./files/deletefiles";

const PORT = 3000;

const main = new Koa();
const router = new Router();

main.use(Json());
main.use(Logger());
main.use(Cors());
main.use(Body());

router.get("/files", async (ctx, next) => {
  ctx.body = await getFileNames();
  await next();
});

router.post("/newCustomFile", async (ctx, next) => {
  ctx.body = await createCustom(ctx.request.body);
  await next();
});

router.post("/newfile", async (ctx, next) => {
  ctx.body = await createFile(ctx.request.body);
  await next();
});

router.post("/deletefiles", async (ctx, next) => {
  ctx.body = await deleteFiles(ctx.request.body);
  await next();
});

router.get("/openfile/:id", async (ctx, next) => {
  ctx.body = await openFile(ctx.params.id);
  await next();
});

router.get("/getfile/:id", async (ctx, next) => {
  ctx.body = await getFile(ctx.params.id);
  await next();
});

router.get("/graph/:file/:type", async (ctx, next) => {
  let message = {};
  switch (ctx.params.type) {
    case "method":
      message = await getMethodGraphData(ctx.params.file);
      break;
    case "status":
      message = await getStatusGraphData(ctx.params.file);
      break;
    case "request":
      message = await getRequestGraphData(ctx.params.file);
      break;
    case "time":
      message = await getTimeGraphData(ctx.params.file);
      break;
    default:
  }
  ctx.body = message;
  await next();
});

main.use(router.routes());
main.use(router.allowedMethods());
main.use(Static("dist"));
main.use(Static("text"));

main.listen(process.env.PORT || PORT, () => {
  console.log(`Listening on port: ${PORT}`);
});
