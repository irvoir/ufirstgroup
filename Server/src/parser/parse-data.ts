const METHODS = ["GET", "POST", "HEAD"];
const PROTOCOLS = ["HTTP/1.0"];

type dateSchema = {
  day: string;
  hour: string;
  minute: string;
  second: string;
};

type jsonSchema = {
  host: string;
  datetime: dateSchema;
  request: {
    method: string;
    url: string;
    protocol: string;
    protocol_version: string;
  };
  response_code: string;
  document_size: string;
};

const defaultSchema = {
  host: null,
  datetime: null,
  request: null,
  response_code: null,
  document_size: null
};

export const parseData = (input: string): jsonSchema => {
  if (input.length === 0) {
    return defaultSchema;
  }
  const objectOrder: string[] = [
    "host",
    "datetime",
    "request",
    "response_code",
    "document_size"
  ];
  const parsedObject: jsonSchema = defaultSchema;
  let item: string = "";
  let request: string = "";
  for (let i = 0; i < input.length; i++) {
    if (input[i].charCodeAt(0) === 34) {
      let saved: number = i++;
      let lastIndex: number = input.length - 1;
      while (lastIndex--) {
        if (input[lastIndex] === '"') break;
      }
      do {
        const current = input[i++];
        const validAscii =
          current.charCodeAt(0) >= 32 && current.charCodeAt(0) <= 126;
        request += validAscii ? current : "";
      } while (i < lastIndex);
      i = saved;
    }
    if (input[i].charCodeAt(0) === 32 && !(input[i + 1].charCodeAt(0) === 32)) {
      let current = objectOrder.shift();
      switch (current) {
        case "host":
          parsedObject["host"] = item;
        case "datetime":
          const dateOrder = ["day", "hour", "minute", "second"];
          const dateObject: dateSchema = {
            day: null,
            hour: null,
            minute: null,
            second: null
          };
          let date: any = item.substring(1, item.length - 1);
          date = date.split(":");
          for (let item of date) {
            dateObject[dateOrder.shift()] = item;
          }
          parsedObject["datetime"] = dateObject;
        case "request":
          const strArr: string[] = [];
          let str = "";
          for (let index = 0; index < request.length; index += 1) {
            if (request[index].charCodeAt(0) === 32) {
              strArr.push(str);
              str = "";
              continue;
            }
            str += request[index];
            if (index === request.length - 1) {
              strArr.push(str);
            }
          }
          let method = null;
          let protocol = null;
          let url = "";

          for (let index = 0; index < strArr.length; index += 1) {
            if (METHODS.includes(strArr[index])) {
              method = strArr[index];
              continue;
            }
            if (PROTOCOLS.includes(strArr[index])) {
              protocol = "HTTP/1.0";
              continue;
            }
            url += strArr[index];
          }
          parsedObject["request"] = {
            method,
            url,
            protocol: protocol ? "HTTP" : null,
            protocol_version: protocol ? "1.0" : null
          };
        default:
      }
      item = "";
    } else {
      item += input[i];
    }
  }
  let responseDocument = [];
  let strParser = [];
  input = input.trimRight();
  for (let index = input.length; index--; ) {
    if (input[index].charCodeAt(0) === 34) {
      break;
    }
    if (input[index].charCodeAt(0) === 32) {
      responseDocument.push(strParser.join(""));
      strParser = [];
      continue;
    }
    strParser.unshift(input[index]);
  }
  parsedObject["response_code"] = responseDocument.pop();
  parsedObject["document_size"] = responseDocument.pop();
  return parsedObject;
};
