import * as fs from "fs";
import { parseData } from "./parse-data";

const INPUT_PATH = "./../epa-http.txt";
const OUTPUT_NAME = "output.json";
const INPUT_OPTIONS = {
  encoding: "utf8",
  highWaterMark: 1024,
}

async function* chunkParserToLine(
  chunksAsync: fs.ReadStream
): AsyncIterableIterator<string> {
  let previous: string = "";
  for await (const chunk of chunksAsync) {
    previous += chunk;
    let eolIndex: number;
    while ((eolIndex = previous.indexOf("\n")) >= 0) {
      const line: string = previous.slice(0, eolIndex + 1);
      yield line;
      previous = previous.slice(eolIndex + 1);
    }
  }
  if (previous.length > 0) {
    yield previous;
  }
}

export const transformData = async (): Promise<boolean> => {
  try {
    let previous: string = "";
    const readStream: fs.ReadStream = fs.createReadStream(INPUT_PATH, INPUT_OPTIONS);
    const writeStream: fs.WriteStream = fs.createWriteStream(OUTPUT_NAME);
    writeStream.write("[\n");
    for await (const line of chunkParserToLine(readStream)) {
      writeStream.write(previous);
      previous = "  " + JSON.stringify(parseData(line), null, 4) + ",\n";
    }
    writeStream.write(previous.substring(0, previous.length - 2));
    writeStream.write("\n]\n");
    writeStream.end();
    return true;
  } catch (error) {
    return false;
  }
};
