import * as fs from 'fs';

export const chunkParserToLine = async function*(
  chunksAsync: fs.ReadStream
): AsyncIterableIterator<string> {
  let previous: string = "";
  for await (const chunk of chunksAsync) {
    previous += chunk;
    let eolIndex: number;
    while ((eolIndex = previous.indexOf("\n")) >= 0) {
      const line: string = previous.slice(0, eolIndex + 1);
      yield line;
      previous = previous.slice(eolIndex + 1);
    }
  }
  if (previous.length > 0) {
    yield previous;
  }
};
