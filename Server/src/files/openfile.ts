import * as fs from "fs";
const fsPromise = fs.promises;
const PATH = "./dist/";

export const openFile = async (file: string) => {
  try {
    let dir = await fsPromise.readFile(PATH + file, 'utf8');
    return JSON.parse(dir);
  } catch (error) {
    return 0;
  }
};
