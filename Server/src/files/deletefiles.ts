import * as fs from "fs";
const fsPromise = fs.promises;
//const PATH = "./dist/";
const PATH = "./";

export const deleteFiles = async (files: string[]) => {
  try {
    for await (let item of files) {
      fsPromise.unlink(PATH + item);
    }
    return {
      message: true
    }
  } catch(error) {
    return {
      message: false
    }
  }
}
