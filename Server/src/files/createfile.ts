import * as fs from "fs";
import { parseData } from "./../parser/parse-data";
import { chunkParserToLine } from "../helpers/helpers";
import { getFileNames } from "../files/getfile";

const PATH = "./dist/";
const INPUT_PATH = "./dist/epa-http.txt";
const INPUT_OPTIONS = {
  encoding: "utf8",
  highWaterMark: 1024
};
interface InputProps {
  name: string;
}

export const createFile = async ({ name }: InputProps): Promise<any> => {
  try {
    const files = await getFileNames();
    if (files.message.length > 7) {
      return {
        message: false
      };
    }
    let previous: string = "";
    const readStream: fs.ReadStream = fs.createReadStream(
      INPUT_PATH,
      INPUT_OPTIONS
    );
    const writeStream: fs.WriteStream = fs.createWriteStream(PATH + name);
    writeStream.write("[\n");
    for await (const line of chunkParserToLine(readStream)) {
      writeStream.write(previous);
      previous = "  " + JSON.stringify(parseData(line), null, 4) + ",\n";
    }
    writeStream.write(previous.substring(0, previous.length - 2));
    writeStream.write("\n]\n");
    writeStream.end();
    return {
      message: true
    };
  } catch (error) {
    return {
      message: false
    };
  }
};

interface OptionsProps {
  name: string;
  range: string;
  paginator: string;
  fields: string;
  pretty: string;
  min: number;
  max: number;
}

export const createCustom = async (options: OptionsProps): Promise<any> => {
  try {
    const files = await getFileNames();
    if (files.message.length > 7) {
      return {
        message: false
      };
    }
    let previous: string = "";
    const readStream: fs.ReadStream = fs.createReadStream(
      INPUT_PATH,
      INPUT_OPTIONS
    );
    const writeStream: fs.WriteStream = fs.createWriteStream(
      PATH + options.name
    );
    let count = 0;
    let { min, max } = options;
    writeStream.write("[\n");
    for await (const line of chunkParserToLine(readStream)) {
      if (options.range && count >= min && count <= max) {
        writeStream.write(previous);
        previous = "  " + JSON.stringify(parseData(line), null, 4) + ",\n";
      } else if (!options.range) {
        writeStream.write(previous);
        previous = "  " + JSON.stringify(parseData(line), null, 4) + ",\n";
      }
      count++;
    }
    writeStream.write(previous.substring(0, previous.length - 2));
    writeStream.write("\n]\n");
    writeStream.end();
    return {
      message: true
    };
  } catch (error) {
    return {
      message: false
    };
  }
};
