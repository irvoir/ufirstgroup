import * as fs from "fs";
import * as path from "path";

const fsPromise = fs.promises;
const PATH = "./dist/";

export const getFileNames = async () => {
  try {
    let dir = await fsPromise.readdir(PATH);
    let files = [];
    for (let index = dir.length; index--; ) {
      if (path.extname(dir[index]) === ".json") {
        files.push(dir[index]);
      }
    }
    return {
      message: files
    };
  } catch (error) {
    return {
      message: []
    };
  }
};

export const getFile = async (file: string) => {
  try {
    let dir = await fsPromise.readFile(file, "utf8");
    return JSON.stringify(dir);
  } catch (error) {
    return 0;
  }
};
