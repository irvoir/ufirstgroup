import * as fs from "fs";
import { chunkParserToLine } from "../helpers/helpers";

const INPUT_OPTIONS = {
  encoding: "utf8",
  highWaterMark: 1024
};

const PATH = './dist/';

export const getMethodGraphData = async (file: string) => {
  try {
    const readStream: fs.ReadStream = fs.createReadStream(PATH + file, INPUT_OPTIONS);
    let found = false;
    let frequency = {};
    for await (const line of chunkParserToLine(readStream)) {
      if (line.includes("request")) {
        found = true;
        continue;
      }
      if (found) {
        let parsed = line.split('"');
        const current = parsed[parsed.length - 2].toUpperCase();
        !!frequency[current] ? frequency[current]++ : (frequency[current] = 1);
        found = false;
      }
    }
    let data = Object.values(frequency);
    let legend = Object.keys(frequency);
    let total = data.reduce((acc: number, curr: number) => acc + curr, 0);
    return {
      message: {
        data,
        legend,
        total
      }
    };
  } catch (error) {
    return {
      message: 0
    };
  }
};

export const getStatusGraphData = async (file: string) => {
  try {
    const readStream: fs.ReadStream = fs.createReadStream(PATH + file, INPUT_OPTIONS);
    let frequency = {};
    for await (const line of chunkParserToLine(readStream)) {
      if (line.includes("response_code")) {
        let parsed = line.split('"');
        const current = parsed[parsed.length - 2].toUpperCase();
        !!frequency[current] ? frequency[current]++ : (frequency[current] = 1);
      }
    }
    let data = Object.values(frequency);
    let legend = Object.keys(frequency);
    let total = data.reduce((acc: number, curr: number) => acc + curr, 0);
    return {
      message: {
        data,
        legend,
        total
      }
    };
  } catch (error) {
    return {
      message: 0
    };
  }
};

export const getRequestGraphData = async (file: string) => {
  try {
    const readStream: fs.ReadStream = fs.createReadStream(PATH + file, INPUT_OPTIONS);
    let frequency = Array.from({ length: 10 }, (k, i) => i * 100 + 100);
    let frequencyObject = {};
    for (let index = 0; index < frequency.length; index += 1) {
      frequencyObject[frequency[index]] = 1;
    }
    let found = false;
    for await (const line of chunkParserToLine(readStream)) {
      if (line.includes("response_code")) {
        let parsed = line.split('"');
        const current = parsed[parsed.length - 2].toUpperCase();
        if (current === "200") {
          found = true;
          continue;
        }
      }
      if (found && line.includes("document_size")) {
        let parsed = line.split('"');
        const current = parsed[parsed.length - 2];
        if (+current < 1000) {
          if (+current < 100) {
            frequencyObject[100]++;
          } else if (+current > 100 && +current < 200) {
            frequencyObject[200]++;
          } else if (+current > 200 && +current < 300) {
            frequencyObject[300]++;
          } else if (+current > 300 && +current < 400) {
            frequencyObject[400]++;
          } else if (+current > 400 && +current < 500) {
            frequencyObject[500]++;
          } else if (+current > 500 && +current < 600) {
            frequencyObject[600]++;
          } else if (+current > 600 && +current < 700) {
            frequencyObject[700]++;
          } else if (+current > 700 && +current < 800) {
            frequencyObject[800]++;
          } else if (+current > 800 && +current < 900) {
            frequencyObject[900]++;
          } else {
            frequencyObject[1000]++;
          }
        }
        found = false;
      }
    }
    let data = Object.values(frequencyObject);
    let total = data.reduce((acc: number, curr: number) => acc + curr, 0);
    return {
      message: {
        data: frequencyObject,
        total
      }
    };
  } catch (error) {
    return {
      message: 0
    };
  }
};

export const getTimeGraphData = async (file: string) => {
  try {
    const readStream: fs.ReadStream = fs.createReadStream(PATH + file, INPUT_OPTIONS);
    let chunk = "";
    let count = 0;
    let found = false;
    let frequency = {};
    for await (const line of chunkParserToLine(readStream)) {
      if (line.includes("datetime")) {
        found = true;
        chunk += "{";
      }
      if (found && count < 6) {
        count++;
        chunk += line;
        continue;
      } else if (count === 6) {
        chunk = chunk.substring(0, chunk.length - 2);
        chunk += "}";
        const {
          datetime: { hour }
        } = JSON.parse(chunk);
        !!frequency[hour] ? frequency[hour]++ : (frequency[hour] = 1);
        chunk = "";
        count = 0;
        found = false;
      }
    }
    let data = Object.values(frequency);
    let total = data.reduce((acc: number, curr: number) => acc + curr, 0);
    return {
      message: {
        data: frequency,
        total
      }
    };
  } catch (error) {
    return {
      message: 0
    };
  }
};
